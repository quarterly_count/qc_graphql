import Db from './connection'
import Sequelize from 'sequelize'

export default Db.define('assignment_relation', {
  assignment_1_id: {
    type: Sequelize.INTEGER
  },
  assignment_2_id: {
    type: Sequelize.INTEGER
  },
  assignment_relation_type_id: {
    type: Sequelize.INTEGER
  }
}, {
  timestamps: false, underscored: true
})
