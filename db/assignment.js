import Db from './connection'
import Sequelize from 'sequelize'

export default Db.define('assignment', {
  name: {
    type: Sequelize.STRING
  },
  label: {
    type: Sequelize.STRING
  },
  assignment_type_id: {
    field: 'assignment_type_id',
    type: Sequelize.INTEGER
  },
}, {timestamps: false, underscored: true})
