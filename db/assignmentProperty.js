import Db from './connection'
import Sequelize from 'sequelize'

export default Db.define('assignment_property', {
  assignment_id: {
    type: Sequelize.INTEGER
  },
  assignment_property_type_id: {
    type: Sequelize.INTEGER
  },
  value: {
    field: 'value',
    type: Sequelize.TEXT,
  }
}, {
  timestamps: false,
  underscored: true
})
