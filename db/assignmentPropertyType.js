import Db from './connection'
import Sequelize from 'sequelize'

export default Db.define('assignment_property_type', {
  name: {
    type: Sequelize.STRING
  },
  label: {
    type: Sequelize.STRING
  }
}, {
  timestamps: false,
  underscored: true
})
