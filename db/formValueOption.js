import Db from './connection'
import Sequelize from 'sequelize'

export default Db.define('form_value_option', {
  form_field_id: {
    type: Sequelize.INTEGER
  },
  /* These are DATETIME WITHOUT TIMEZONE:
  created_at: { },
  updated_at: { }
  */
  value: {
    type: Sequelize.STRING
  },
  assignment_id: {
    type: Sequelize.INTEGER
  }
}, {
  timestamps: false,
  underscored: true
})
