import Db from './connection'
import Sequelize from 'sequelize'

export default Db.define('assignment_type', {
  name: {
    type: Sequelize.STRING
  },
  label: {
    type: Sequelize.STRING
  },
  description: {
    type: Sequelize.TEXT
  }
}, {
  timestamps: false,
  underscored: true
})
