import Db from './connection'
import AssignmentModel from './assignment'
import AssignmentTypeModel from './assignmentType'
import AssignmentPropertyModel from './assignmentProperty'
import AssignmentPropertyTypeModel from './assignmentPropertyType'
import AssignmentRelationModel from './assignmentRelation'
import AssignmentRelationTypeModel from './assignmentRelationType'
import FormValueOptionModel from './formValueOption'

AssignmentModel.belongsTo(AssignmentTypeModel) // => assignment.getAssignment_type()

Db.sync()

export {
  Db,
  
  AssignmentModel,
  AssignmentTypeModel,
  
  AssignmentPropertyModel,
  AssignmentPropertyTypeModel,

  AssignmentRelationModel,
  AssignmentRelationTypeModel,

  FormValueOptionModel
}