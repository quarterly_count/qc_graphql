import Sequelize from 'sequelize'
import dotenv from 'dotenv'

dotenv.config()

export default new Sequelize(
  process.env.DATABASE_NAME,
  process.env.DB_USERNAME,
  process.env.DB_PASSWORD,
  {
    dialect: 'postgres',
    host: process.env.DATABASE_URL
  }
)