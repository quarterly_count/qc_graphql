import Db from './connection'
import Sequelize from 'sequelize'

export default Db.define('assignment_relation_type', {
  name: {
    type: Sequelize.STRING
  },
  description: {
    type: Sequelize.TEXT
  },
  assignment_1_type_id: {
    type: Sequelize.INTEGER
  },
  assignment_2_type_id: {
    type: Sequelize.INTEGER
  }
}, {
  timestamps: false,
  underscored: true
})
