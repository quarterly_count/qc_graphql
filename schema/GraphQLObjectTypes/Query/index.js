import { GraphQLObjectType } from 'graphql'

import fields from './fields'

export default new GraphQLObjectType({
  name: 'query',
  description: 'The top level GraphQLObjectType',
  fields: () => fields,
})
