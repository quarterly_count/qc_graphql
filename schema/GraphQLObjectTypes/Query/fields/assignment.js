import { GraphQLString, GraphQLInt } from 'graphql'
import { Db, AssignmentModel } from '../../../../db'
import Assignment from '../../Assignment'


export default {
  type: Assignment,
  args: {
    type: { type: GraphQLString },
    id: { type: GraphQLInt }
  },
  resolve(root, args) {
     let sqlString
     sqlString = `
       SELECT asgs.id,
               asgs.name as name,
               asgs.label as label,
               asgs.assignment_type_id as assignment_type_id
       FROM assignments asgs
       JOIN assignment_types ats
         ON asgs.assignment_type_id = ats.id
       WHERE ats.name = '${args.type}'
       AND asgs.id = '${args.id}'
      `
    return Db.query(sqlString, { model: AssignmentModel, type: Db.QueryTypes.SELECT })
             .then(assignments => assignments[0])
  }
}
