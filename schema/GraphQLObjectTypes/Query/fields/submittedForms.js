import {
  GraphQLString,
  GraphQLInt,
} from 'graphql'

import { Db } from '../../../../db'

export default {
  type: GraphQLInt,
  args: {
    filterType: { type: GraphQLString },
    id: { type: GraphQLInt }
  },
  resolve(root, args){
    const allSurveysString = `
      WITH all_surveys AS (
        SELECT DISTINCT sfs.id as id
        FROM submitted_forms sfs
        JOIN form_values fvs
          ON sfs.id = fvs.submitted_form_id
        JOIN form_value_options fvos
          ON fvs.form_value_option_id = fvos.id
        JOIN assignments asgs
          ON fvos.assignment_id = asgs.id
        JOIN assignment_types ats
          ON asgs.assignment_type_id = ats.id
        WHERE ats.name = 'formTypeCategory'
        AND asgs.name = 'survey'
      )
    `
    let sqlString
    if (args.filterType === 'all') {
      sqlString = `
      ${allSurveysString}
      SELECT COUNT(id)
      FROM all_surveys
    `
    } else {
      sqlString = `
      ${allSurveysString}
      SELECT COUNT(DISTINCT all_surveys.id)
      FROM all_surveys
      JOIN form_values fvs
        ON all_surveys.id = fvs.submitted_form_id
      JOIN form_value_options fvos
        ON fvs.form_value_option_id = fvos.id
      JOIN assignments asgs
        ON fvos.assignment_id = asgs.id
      JOIN assignment_types ats
        ON asgs.assignment_type_id = ats.id
      WHERE ats.name = '${args.filterType}'
      AND asgs.id = ${args.id}`
    }
    return Db.query(sqlString).then(result => result[0][0]['count'])
  }
}
