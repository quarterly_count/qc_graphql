import submittedForms from './submittedForms';
import assignment from './assignment';
import assignments from './assignments';

export default {
  submittedForms,
  assignment,
  assignments,
}
