import { GraphQLString, GraphQLList } from 'graphql'
import { Db, AssignmentModel } from '../../../../db'
import Assignment from '../../Assignment'

export default {
  type: new GraphQLList(Assignment),
  args: {
    type: { type: GraphQLString }
  },
  resolve(root, args) {
   const sqlString =
     `SELECT asgs.id,
             asgs.name as name,
             asgs.label as label,
             asgs.assignment_type_id as assignment_type_id
     FROM assignments asgs
     JOIN assignment_types ats
       ON asgs.assignment_type_id = ats.id
    WHERE ats.name = '${args.type}'
    `
    return Db.query(sqlString, { model: AssignmentModel, type: Db.QueryTypes.SELECT })
            .then(result => {
              console.log('result', JSON.stringify(result))
              return result
            })
  }
}
