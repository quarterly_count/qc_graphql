import {
	GraphQLObjectType,
	GraphQLInt
} from 'graphql'

export default new GraphQLObjectType({
	name: 'AssignmentRelation',
	description: 'Models the relationship between two assignments',
	fields: () => { return {
		id: {
			type: GraphQLInt,
			resolve(relation) { return relation.id }
		},
		assignment1Id: {
			type: GraphQLInt,
			resolve(relation) { return relation.assignment_1_id }
		},
		assignment2Id: {
			type: GraphQLInt,
			resolve(relation) { return relation.assignment_2_id }
		},
		assignmentRelationTypeId: {
			type: GraphQLInt,
			resolve(relation) { return relation.assignment_relation_type_id }
		}
	} }
})