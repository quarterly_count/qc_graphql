import {
	GraphQLInputObjectType,
	GraphQLString,
	GraphQLNonNull
} from 'graphql'

export default new GraphQLInputObjectType({
	name: 'UpdateAssignmentPropertyInput',
	description: 'An input type specific to updating assignment properties; it disallows changing the assignment property type.',

	fields: () => ({
		type:  { type: new GraphQLNonNull(GraphQLString) },
		newValue: { type: GraphQLString },
		newLabel: { type: GraphQLString }
	})
})