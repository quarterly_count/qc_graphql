import {
	GraphQLInputObjectType,
	GraphQLString,
	GraphQLList,
	GraphQLNonNull
} from 'graphql'
import AssignmentPropertyInput from './assignmentProperty'

export default new GraphQLInputObjectType({
	name: 'AssignmentInput',
	description: 'An assignment input type specifically for creation; it cannot be used to update an assignment.',

	fields: () => ({
		name:  		{ type: new GraphQLNonNull(GraphQLString) },
		type: 		{ type: new GraphQLNonNull(GraphQLString) },
		properties: { type: new GraphQLList(AssignmentPropertyInput) },
		label: 		{ type: GraphQLString }
	})
})