import {
	GraphQLInputObjectType,
	GraphQLString,
	GraphQLNonNull
} from 'graphql'

export default new GraphQLInputObjectType({
	name: 'AssignmentPropertyInput',
	description: 'Describes the value and type of a property, with an optional label',

	fields: () => ({
		type:  { type: new GraphQLNonNull(GraphQLString) },
		value: { type: new GraphQLNonNull(GraphQLString) },
		label: { type: GraphQLString }
	})
})