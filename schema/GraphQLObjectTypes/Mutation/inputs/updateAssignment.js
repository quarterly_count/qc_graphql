import {
	GraphQLInputObjectType,
	GraphQLString,
	GraphQLList
} from 'graphql'
import UpdateAssignmentPropertyInput from './updateAssignmentProperty'

export default new GraphQLInputObjectType({
	name: 'UpdateAssignmentInput',
	description: 'An input type specific to updating assignments; it disallows changing the assignment type.',

	fields: () => ({
		name:  		{ type: GraphQLString },
		label: 		{ type: GraphQLString },
		properties: { type: new GraphQLList(UpdateAssignmentPropertyInput) }
	})
})