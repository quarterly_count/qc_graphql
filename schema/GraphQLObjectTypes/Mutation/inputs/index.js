import AssignmentInput from './assignment'
import AssignmentPropertyInput from './assignmentProperty'

export {
	AssignmentInput,
	AssignmentPropertyInput
}