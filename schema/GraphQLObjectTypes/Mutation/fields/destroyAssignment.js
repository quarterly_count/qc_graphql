import {
	AssignmentModel, AssignmentTypeModel,
	AssignmentRelationModel, AssignmentPropertyModel,
	FormValueOptionModel
} from '../../../../db'
import {
	GraphQLBoolean, GraphQLNonNull, GraphQLInt
} from 'graphql'

export default {
	type: GraphQLBoolean,

	description:
`Destroy an assignment. Any assignment relations and assignment properties
involving this assignment will also be destroyed.`,

	args: {
		assignmentId: { type: new GraphQLNonNull(GraphQLInt) }
	},
	resolve(value, { assignmentId }) {
		return AssignmentModel.findAll({
			include: [AssignmentTypeModel],
			where: { id: assignmentId },
			limit: 1
		}).spread(assignment => {
			if (!assignment) throw new Error(`No assignment with ID ${assignmentId} exists`)

			const allowedAssignmentTypes = ['user', 'team']
			const typeName = assignment.assignment_type.name
			if (!allowedAssignmentTypes.includes(typeName)) {
				throw new Error(`Only assignments of ${allowedAssignmentTypes.join('/')} type may be destroyed. Assignment ${assignmentId} is ${typeName} type.`)
			}

			return Promise.all([
				AssignmentRelationModel.destroy({ where: {
					$or: [{
						assignment_1_id: assignmentId
					}, {
						assignment_2_id: assignmentId
					}]
				}}),
				AssignmentPropertyModel.destroy({ where: {
					assignment_id: assignmentId
				}}),
				FormValueOptionModel.update({
					assignment_id: null
				}, { where: {
					assignment_id: assignmentId
				}})
			]).then(([numRelations, numProperties, numOptions]) => {
				console.log(`${numRelations} assignment relations destroyed`)
				console.log(`${numProperties} assignment properties destroyed`)
				console.log(`${numOptions} form value options updated`)

				return AssignmentModel.destroy({ where: {
					id: assignmentId
				}}).then(numAssignmentsDestroyed => {
					console.log(`Assignment ${assignmentId} destroyed`)
					return true
				})
			})
		})
	}
}