import createAssignment from './createAssignment'
import updateAssignment from './updateAssignment'
import destroyAssignment from './destroyAssignment'

import createAssignmentRelation from './createAssignmentRelation'
import destroyAssignmentRelation from './destroyAssignmentRelation'

export default {
	createAssignment,
	updateAssignment,
	destroyAssignment,

	createAssignmentRelation,
	destroyAssignmentRelation
}