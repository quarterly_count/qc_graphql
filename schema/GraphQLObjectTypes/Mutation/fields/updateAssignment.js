import Assignment from '../../Assignment'
import UpdateAssignmentInput from '../inputs/updateAssignment'
import {
	AssignmentModel, AssignmentTypeModel,
	AssignmentPropertyModel, AssignmentPropertyTypeModel
} from '../../../../db'
import { GraphQLInt, GraphQLNonNull } from 'graphql'

export default {
	type: Assignment,
	description: 'Update an assignment.',
	args: {
		id: 	{ type: new GraphQLNonNull(GraphQLInt) },
		update: { type: new GraphQLNonNull(UpdateAssignmentInput) }
	},
	resolve(value, { id, update }) {
		console.log('update.properties', JSON.stringify(update.properties))
		const updatePropertyPromises = (update.properties || []).map(propertyInput => {
			return new Promise((resolve, reject) => {
				if (!(('newValue' in propertyInput) || ('newLabel' in propertyInput))) {
					reject(new Error('newValue and/or newLabel inputs are required to update or create properties'))
				}

				const propertyTypeDefaults = 'newLabel' in propertyInput ? { label: propertyInput.newLabel } : {}
				AssignmentPropertyTypeModel.findOrCreate({
					where: { name: propertyInput.type },
					defaults: propertyTypeDefaults
				}).spread((propertyType, wasCreated) => {
					let propertyTypeUpdates = {}
					console.log('propertyType', JSON.stringify(propertyType))
					console.log('was created', wasCreated)
					if (!wasCreated && ('newLabel' in propertyInput) && propertyInput.newLabel !== propertyType.label) {
						propertyTypeUpdates.label = propertyInput.newLabel
					}
					propertyType.update(propertyTypeUpdates).then(propertyType => {
						const propertyDefaults = {}
						if ('newValue' in propertyInput) propertyDefaults.value = propertyInput.newValue
						AssignmentPropertyModel.findOrCreate({
							where: { assignment_property_type_id: propertyType.id, assignment_id: id },
							defaults: propertyDefaults
						}).spread((property, wasCreated) => {
							console.log('property', JSON.stringify(property))
							console.log('property wasCreated', wasCreated)
							let propertyUpdates = {}
							if (!wasCreated && 'newValue' in propertyInput && propertyInput.newValue !== property.value) {
								propertyUpdates.value = propertyInput.newValue
							}
							property.update(propertyUpdates).then(
								resolve()
							).catch(e => { reject(e) })
						}).catch(e => { reject(e) })
					}).catch(e => { reject(e) })
				}).catch(e => { reject(e) })
			})
		})

		return new Promise((resolve, reject) => {
			AssignmentModel.findById(id).then(assignment => {
				if (!assignment) reject(new Error(`No assignment was found with ID ${id}`))

				let assignmentUpdates = {}
				if ('name' in update && update.name !== assignment.name) assignmentUpdates.name = update.name
				if ('label' in update && update.label !== assignment.label) assignmentUpdates.label = update.label
				assignment.update(assignmentUpdates).then(() => {
					Promise.all(updatePropertyPromises).then(() => {
						resolve(assignment)
					}).catch(e => {
						reject(e)
					})
				}).catch(e => { reject(e)  })
			}).catch(e => { reject(e) })
		})
	}
}
