import {
	AssignmentRelationModel, AssignmentRelationTypeModel
} from '../../../../db'
import {
	GraphQLInt, GraphQLNonNull, GraphQLString, GraphQLBoolean
} from 'graphql'

export default {
	type: GraphQLBoolean,

	description:
`Destroy a relationship between two assignments. Its arguments are
the same as createAssignmentRelation, so you should use them in the
same fashion.`,

	args: {
		parentAssignmentId: { type: new GraphQLNonNull(GraphQLInt) },
		childAssignmentId: { type: new GraphQLNonNull(GraphQLInt) }
	},
	resolve(value, { childAssignmentId, parentAssignmentId }) {
		return AssignmentRelationModel.findOne({
			where: {
				assignment_1_id: childAssignmentId,
				assignment_2_id: parentAssignmentId
			}
		}).then(relation => {
			if (!relation) throw new Error(`No assignment relation with parent assignment ID ${parentAssignmentId} and child ${childAssignmentId} exists`)
			return relation.destroy().then(() => true)
		})
	}
}