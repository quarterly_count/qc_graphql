import Assignment from '../../Assignment'
import { AssignmentInput } from '../inputs'
import {
	AssignmentModel, AssignmentTypeModel,
	AssignmentPropertyModel, AssignmentPropertyTypeModel
} from '../../../../db'
import { GraphQLNonNull } from 'graphql'

export default {
	type: Assignment,

	description:
`Create a new assignment. If the assignment type does not exist, it will be created;
duplicate assignment types are not allowed. However, duplicate assignments are allowed.
If the assignment property type does not exist, it will be created; duplicate assignment
types are not allowed. However, duplicate properties are allowed.
`,

	args: {
		assignment: { type: new GraphQLNonNull(AssignmentInput) }
	},
	resolve(value, args) {
		const assignmentInput = args.assignment

		return new Promise((resolve, reject) => {
			// 1. Look up or create the assignment type, which is required for the assignment
			AssignmentTypeModel.findOrCreate({
				where: { name: assignmentInput.type }
			}).spread(function(assignmentType, wasCreated) {
				// 2. Create the assignment, relating it to the above type
				AssignmentModel.create({
					name: assignmentInput.name,
					label: assignmentInput.label,
					assignment_type_id: assignmentType.id
				}).then(function(assignment) {
					// 3. For each provided property...
					const createPropertyPromises = (assignmentInput.properties || []).map(propertyInput => {
						return new Promise((resolve, reject) => {
							// 3.1. Find or create the given property type
							AssignmentPropertyTypeModel.findOrCreate({
								where: { name: propertyInput.type },
								defaults: { label: propertyInput.label },
								limit: 1
							}).spread(function(propertyType, wasCreated) {
								// 3.2. Create the property, relating it to the type and assignment
								AssignmentPropertyModel.create({
									value: propertyInput.value,
									assignment_id: assignment.id,
									assignment_property_type_id: propertyType.id
								}).then(
									resolve
								).catch(e => { reject(e) })
							}).catch(e => { reject(e) })
						})
					})

					Promise.all(createPropertyPromises).then(() => {
						resolve(assignment)
					}).catch(e => { reject(e) })
				}).catch(e => { reject(e) })
			}).catch(e => { reject(e) })
		})
	}
}
