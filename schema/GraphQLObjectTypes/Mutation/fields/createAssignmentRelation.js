import AssignmentRelation from '../../AssignmentRelation'
import {
	AssignmentModel, AssignmentTypeModel,
	AssignmentRelationModel, AssignmentRelationTypeModel
} from '../../../../db'
import { GraphQLInt, GraphQLNonNull } from 'graphql'
import Sequelize from 'sequelize'

export default {
	type: AssignmentRelation,

	description:
`Create a parent/child, has/belongs-to relationship between two assignments.
The parent assignment is the one that "has" the child assignments. Conversely,
the child assignment "belongs to" the parent. For instance, if there was a user
to be assigned to a team, the parent would be the team and the child would be
the user — a team "has" a user and a user "belongs to" a team.`,

	args: {
		parentAssignmentId: { type: new GraphQLNonNull(GraphQLInt) }, // assignment1
		childAssignmentId: 	{ type: new GraphQLNonNull(GraphQLInt) }  // assignment2
	},
	resolve(value, { parentAssignmentId, childAssignmentId }) {
		// 1. Find both assignment models, joining the types
		return AssignmentModel.findAll({
			include: [AssignmentTypeModel],
			where: {
				$or: [{ id: parentAssignmentId }, { id: childAssignmentId }]
			}
		}).spread(function(parent, child) {
			// 2. Ensure both assignments exist
			if (!parent) throw new Error(`No parent assignment exists with ID ${parentAssignmentId}`)
			if (!child) throw new Error(`No child assignment exists with ID ${childAssignmentId}`)

			const parentType = parent.assignment_type
			const childType = child.assignment_type

			// 3. Ensure both types exist
			if (!parentType) throw new Error(`No assignment type exists for parent assignment with ID ${parentAssignmentId}`)
			if (!childType) throw new Error(`No assignment type exists for child assignment with ID ${childAssignmentId}`)

			// 4. Find or create a relation type with a cute name in the event it's new
			const defaultName = `${childType.name[0].toLocaleUpperCase() + childType.name.substr(1)} to ${parentType.name[0].toLocaleUpperCase() + parentType.name.substr(1)}`
			return AssignmentRelationTypeModel.findOrCreate({
				where: {
					assignment_1_type_id: childType.id,
					assignment_2_type_id: parentType.id
				},
				defaults: {
					name: defaultName
				}
			}).spread(function(relationType, wasCreated) {
				// 5. Find or create the relation given all the above data
				return AssignmentRelationModel.findOrCreate({
					where: {
						assignment_1_id: childAssignmentId,
						assignment_2_id: parentAssignmentId,
						assignment_relation_type_id: relationType.id
					}
				}).spread(function(relation, wasCreated) {
					return relation
				})
			})
		})
	}
}