import { GraphQLObjectType } from 'graphql'
import fields from './fields'

export default new GraphQLObjectType({
	name: 'mutation',
	description: 'The mutation object type',
	fields: () => fields
})