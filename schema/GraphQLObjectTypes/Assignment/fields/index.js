import id from './id'
import name from './name'
import label from './label'
import assignmentProperty from './assignmentProperty'

export default {
  id,
  name,
  label,
  assignmentProperty,
}
