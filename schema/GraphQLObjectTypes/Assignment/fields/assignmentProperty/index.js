import { GraphQLString } from 'graphql'
import { Db, AssignmentPropertyModel } from '../../../../../db'
import { assignmentStatus, assignmentLocation } from './queryStringGenerators'

export default {
  type: GraphQLString,
  args: {
    type: { type: GraphQLString }
  },
  resolve(assignment, args) {
    let sqlString;
    if (args.type === 'route_status') {
      sqlString = assignmentStatus({assignmentId: assignment.id})

      return Db.query(sqlString, {type: Db.QueryTypes.SELECT})
              .then(result => {
                console.log('result', result)
                return result[0].status
              })
    } else if ((args.type === 'latitude') || (args.type === 'team_latitude')) {
      sqlString = assignmentLocation({assignmentId: assignment.id})
      return Db.query(sqlString, {type: Db.QueryTypes.SELECT})
              .then(result => {
                  const x= result[0]
                  return x.latitude
                })
    } else if ((args.type === 'longitude') || (args.type === 'team_longitude')) {
     sqlString = assignmentLocation({assignmentId: assignment.id})
     return Db.query(sqlString, {type: Db.QueryTypes.SELECT})
             .then(result => {
                const x= result[0]
                 return x.longitude
               })
   } else {
      sqlString = `
        SELECT * FROM assignment_properties aps
        JOIN assignment_property_types apts
        ON aps.assignment_property_type_id = apts.id
        WHERE apts.name = '${args.type}'
        AND aps.assignment_id = ${assignment.id}
      `
      return Db.query(sqlString, { model: AssignmentPropertyModel, type: Db.QueryTypes.SELECT })
               .then(function(assignmentProperties) {
                 // FIXME : below is a hack
                 return JSON.parse(JSON.stringify(assignmentProperties[0])).value
               })
    }
  }
}
