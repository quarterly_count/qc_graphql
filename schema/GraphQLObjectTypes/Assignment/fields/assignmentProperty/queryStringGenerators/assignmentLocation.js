/*

1. connect assignment to most recently submitted_form
  1. join assignments to sfs, group by assignment to get max created_at
  2. join result1 back to sfs on assignment_id, created_at to get sfid

2. get coordinates associated to submitted_form

*/

const assignmentLocation = args => {
  return `

  WITH table1 AS (
    SELECT
      asgs.id as assignment_id,
      sfs.id as submitted_form_id,
      fvs2.datetime_value as submitted_form_created_at
    FROM assignments asgs
    JOIN form_value_options fvos1
      ON asgs.id = fvos1.assignment_id
    JOIN form_values fvs1
      ON fvos1.id = fvs1.form_value_option_id
    JOIN submitted_forms sfs
      ON fvs1.submitted_form_id = sfs.id

    JOIN form_values fvs2
      ON sfs.id = fvs2.submitted_form_id
    JOIN form_fields ffs1
      ON fvs2.form_field_id = ffs1.id


    WHERE asgs.id = ${args.assignmentId}
      AND ffs1.name = 'submittedAt'
  ),

  table2 AS (
    SELECT
      MAX(table1.submitted_form_created_at) as submitted_form_created_at
    FROM table1
  ),

  table3 AS (
    SELECT
      table1.submitted_form_id as submitted_form_id
    FROM table1
    JOIN table2
      ON table1.submitted_form_created_at = table2.submitted_form_created_at
  )

  SELECT
    fvs1.string_value as latitude,
    fvs2.string_value as longitude
  FROM table3
  JOIN form_values fvs1
    ON table3.submitted_form_id = fvs1.submitted_form_id
  JOIN form_fields ffs1
    ON fvs1.form_field_id = ffs1.id

  JOIN form_values fvs2
    ON table3.submitted_form_id = fvs2.submitted_form_id
  JOIN form_fields ffs2
    ON fvs2.form_field_id = ffs2.id

  WHERE ffs1.name = 'latitude'
    AND ffs2.name = 'longitude'
  `
}

export default assignmentLocation
