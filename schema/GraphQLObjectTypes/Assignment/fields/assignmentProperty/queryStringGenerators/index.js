import assignmentLocation from './assignmentLocation'
import assignmentStatus from './assignmentStatus'

export {
  assignmentLocation,
  assignmentStatus,
}
