import { GraphQLString } from 'graphql'

export default {
  type: GraphQLString,
  resolve(assignment) {
    return assignment.name
  }
}
