import { GraphQLInt } from 'graphql'

export default {
  type: GraphQLInt,
  resolve(assignment) {
    return assignment.id
  }
}
