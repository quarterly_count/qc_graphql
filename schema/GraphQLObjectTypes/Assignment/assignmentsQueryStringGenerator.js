const assignmentAssignmentsByType = args => {
  let sqlPart2;
  sqlPart2 = `
    SELECT ids.id as id,
           asgs.name as name,
           asgs.label as label,
           asgs.assignment_type_id as assignment_type_id
    FROM ids
    JOIN assignments asgs
    ON ids.id = asgs.id`

  return `

WITH

correct_relation_types AS (
  SELECT arts.id as id,
         ats1.id as assignment_1_type_id,
         ats2.id as assignment_2_type_id
  FROM assignment_relation_types arts
  JOIN assignment_types ats1
    ON arts.assignment_1_type_id = ats1.id
  JOIN assignment_types ats2
    ON arts.assignment_2_type_id = ats2.id
  WHERE (
        ats1.id = ${args.originAssignmentTypeId}
    AND ats2.name = '${args.targetAssignmentTypeName}'
  )
  OR (
        ats1.name = '${args.targetAssignmentTypeName}'
    AND ats2.id = ${args.originAssignmentTypeId}
  )
),

ids AS (

 SELECT
  CASE
    WHEN correct_relation_types.assignment_1_type_id = ${args.originAssignmentTypeId}
         AND ars.assignment_1_id = ${args.originAssignmentId}
    THEN ars.assignment_2_id
    ELSE ars.assignment_1_id
    END
  as id

 FROM assignment_relations ars
 JOIN correct_relation_types
 ON ars.assignment_relation_type_id = correct_relation_types.id

 WHERE
    (
      correct_relation_types.assignment_1_type_id = ${args.originAssignmentTypeId}
      AND ars.assignment_1_id = ${args.originAssignmentId}
    )
  OR
    (
      correct_relation_types.assignment_2_type_id = ${args.originAssignmentTypeId}
      AND ars.assignment_2_id = ${args.originAssignmentId}
    )
)

${sqlPart2}
`
}

export default assignmentAssignmentsByType
