import { GraphQLObjectType, GraphQLString, GraphQLList, } from 'graphql'
import fields from './fields'
import { Db, AssignmentModel } from '../../../db'
import assignmentsQueryStringGenerator from './assignmentsQueryStringGenerator'

const Assignment =  new GraphQLObjectType({
  name: 'assignment',
  fields: () => Object.assign({}, fields, {
  	altAssignments: {
  		args: { type: { type: GraphQLString } },
  		type: new GraphQLList(Assignment),
  		resolve(parentAssignment, { type }) {
  			const sql = queryStringForChildAssignments(parentAssignment, type)
  			return Db.query(sql, { model: AssignmentModel, type: Db.QueryTypes.SELECT })
  		}
  	},

    // unfortunately we have to define the 'assignments' field in this file, since it's recursive
    // (it's return type is itself a list of Assignment)
    assignments: {
      args: {
        type: { type: GraphQLString }
      },
      type: new GraphQLList(Assignment),
      resolve(assignment, args) {
        const queryArgs = {
          originAssignmentId: assignment.id,
          originAssignmentTypeId: assignment.assignment_type_id,
          targetAssignmentTypeName: args.type,
        }
        const sqlString = assignmentsQueryStringGenerator(queryArgs)
        return Db.query(sqlString, { model: AssignmentModel, type: Db.QueryTypes.SELECT })
      }
    }
  }),
})

const queryStringForChildAssignments = function(parentAssignment, childTypeName) {
	return `
WITH child_assignment_type AS (
	/* 1. Look up the child's assignment type using the given name */
	SELECT id FROM assignment_types
	WHERE name = '${childTypeName}'
), bidirectional_assignment_relation_type_ids AS (
	/*
		2. Look up the assignment relation types that are either
		parent -> child or child -> parent. This gives us a criterion
		with which to filter the assignment relations by type.
	*/
	SELECT assignment_relation_types.id FROM assignment_relation_types
	JOIN child_assignment_type
	ON
		(assignment_relation_types.assignment_1_type_id = child_assignment_type.id
			OR
		assignment_relation_types.assignment_2_type_id = child_assignment_type.id)
		AND
		(assignment_relation_types.assignment_1_type_id = ${parentAssignment.assignment_type_id}
			OR
		assignment_relation_types.assignment_2_type_id = ${parentAssignment.assignment_type_id})
), qualifying_assignment_relations AS (
	/*
		3. Find all of the assignment relations that relate the parent
		assignment to another with one of the qualifying relation types.
	*/
	SELECT assignment_1_id, assignment_2_id FROM assignment_relations
	JOIN bidirectional_assignment_relation_type_ids
	ON assignment_relations.assignment_relation_type_id = bidirectional_assignment_relation_type_ids.id
	WHERE assignment_1_id = ${parentAssignment.id} OR assignment_2_id = ${parentAssignment.id}
)

/*
	4. Find all of the qualifying assignments of the child
	assignment type, being sure to ignore the parent assignment
*/
SELECT assignments.* FROM assignments
JOIN qualifying_assignment_relations
ON
	qualifying_assignment_relations.assignment_1_id = assignments.id OR
	qualifying_assignment_relations.assignment_2_id = assignments.id
JOIN child_assignment_type
ON  child_assignment_type.id = assignments.assignment_type_id
WHERE assignments.id != ${parentAssignment.id};
`}

export default Assignment
