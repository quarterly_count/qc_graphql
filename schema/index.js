import { GraphQLSchema } from 'graphql'
import Query from './GraphQLObjectTypes/Query'
import Mutation from './GraphQLObjectTypes/Mutation'

const Schema = new GraphQLSchema({
	query: Query,
	mutation: Mutation
})

export default Schema
