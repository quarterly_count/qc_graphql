
### What is GraphQL

Explanation of what it is and why it exists :
https://code.facebook.com/posts/1691455094417024

This is a good graphql tutorial, on which this server was originally based :
https://www.youtube.com/watch?v=DNPVqK_woRQ

These are some extra steps I had to take to get the tutorial to work :

1. install babel-cli
`npm install -g babel-cli`

(note: steps 2 and 3 don't apply to the current version of this app, which points to a remote database, but are still useful to getting the version of the app built that is specified in the tutorial)

2. create 'relay' database

`cd /usr/local/bin`

`createdb -h localhost -p 5432 -U MY_USERNAME relay`

(http://www.tutorialspoint.com/postgresql/postgresql_create_database.htm)

3. change the second and third parameters in Sequelize.new from 'postgres', 'postgres', to 'MY_USERNAME', 'MY_USER_PASSWORD'

#### Install Packages

`npm install`

#### Run
`npm start`

#### Try out graphql queries
Navigate to `localhost:3000/graphql`, where you can try different graphql queries. (Example - http://localhost:3000/graphql?query=%7B%0A%09people%20%7B%0A%20%20%20%20firstName%0A%20%20%20%20posts%20%7B%0A%20%20%20%20%20%20title%0A%20%20%20%20%20%20person%20%7B%0A%20%20%20%20%20%20%20%20firstName%20%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D)
