#!/bin/bash
set -e -x
# rest of the script
#!/bin/bash
set -e -x
# rest of the script
echo "Building DHS GraphQL in $DHS_ENV"
AGENT_INSTALL_DIR="/var/lib/go-agent/pipelines"
WORKSPACE="$AGENT_INSTALL_DIR/$GO_PIPELINE_NAME"
echo "workspace is $WORKSPACE"
echo "environment is $GO_ENVIRONMENT_NAME"
DOCKER_HOME="$WORKSPACE/docker"



npm install


#docker -H localhost:2375 build --rm -t devopulence/mysql:sif $DOCKER_HOME
echo "DHS_ENV = $DHS_ENV"
echo "DHS_DB_HOST_NAME = $DHS_DB_HOST_NAME"
if [ $DHS_ENV = "development" ] || [ $DHS_ENV = "production" ]; then
   echo "development"
   sed -i -e "s/quarterlycountprod01.cpgmfgagxzbe.us-east-1.rds.amazonaws.com/$DHS_DB_HOST_NAME/g" .env
else
  echo "non development"
fi
