#!/bin/bash
set -e -x
# rest of the script
#!/bin/bash
set -e -x
# rest of the script
echo 'Building SIF Project'
AGENT_INSTALL_DIR="/var/lib/go-agent/pipelines"
WORKSPACE="$AGENT_INSTALL_DIR/$GO_PIPELINE_NAME"
echo "workspace is $WORKSPACE"
DOCKER_HOME="$WORKSPACE/docker"



#pm2 start app.js -n npm
if (pm2 list -m | grep "npm") ; then
    echo "npm installed"
    pm2 stop npm
else
    echo "npm not installed do nothing"
fi
