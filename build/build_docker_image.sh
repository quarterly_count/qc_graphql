#!/bin/bash
set -e -x
# rest of the script
#!/bin/bash
set -e -x
# rest of the script
echo 'Building Docker Container'
AGENT_INSTALL_DIR="/var/lib/go-agent/pipelines"
WORKSPACE="$AGENT_INSTALL_DIR/$GO_PIPELINE_NAME"
echo "workspace is $WORKSPACE"
DOCKER_HOME="$WORKSPACE"
echo "Docker home is $DOCKER_HOME"
echo "Docker Image Name is  $DOCKER_IMAGE_NAME"
echo "Docker Container Name is  $DOCKER_CONTAINER_NAME"

#docker -H localhost:2375 build --rm -t devopulence/mysql:sif $DOCKER_HOME
echo "DHS_ENV = $DHS_ENV"
if [ $DHS_ENV = "development" ]; then
   echo "development"
   sed -i -e 's/quarterlycountprod01/quarterlycountdev02/g' .env
else
  echo "non development"
fi

sudo docker build --no-cache  --rm  -t $DOCKER_IMAGE_NAME $DOCKER_HOME
