#!/bin/bash
set -e -x
# rest of the script
#!/bin/bash
set -e -x
# rest of the script
echo 'Running Docker Container'
AGENT_INSTALL_DIR="/var/lib/go-agent/pipelines"
WORKSPACE="$AGENT_INSTALL_DIR/$GO_PIPELINE_NAME"
echo "workspace is $WORKSPACE"
DOCKER_HOME="$WORKSPACE/docker"
echo "Docker home is $DOCKER_HOME"
echo "Docker Image Name is  $DOCKER_IMAGE_NAME"
echo "Docker Container Name is  $DOCKER_CONTAINER_NAME"

containers=$(sudo docker ps -a -f name=$DOCKER_CONTAINER_NAME -q)
if [[ $? != 0 ]]; then
    echo "Command failed."
    exit 1
elif [[ $containers ]]; then
    echo "We found containers do nothing"

else
    echo "No containers found start"
sudo docker run -d --name $DOCKER_CONTAINER_NAME -p 3000:3000 $DOCKER_IMAGE_NAME
fi
